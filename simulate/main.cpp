#include <Eigen/Dense>
#include <Slimage/IO.hpp>
#include <Slimage/Gui.hpp>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <boost/math/constants/constants.hpp>
#include <random>
#include <iostream>

struct Box
{
	Eigen::Vector3f position;
	Eigen::Quaternionf orientation;
	Eigen::Vector3f size;
	Eigen::Vector3f color;
};

std::vector<Box> CreateBoxWand(unsigned int nw, unsigned int nh, const Eigen::Vector3f& box_size, float depth_var_sigma)
{
	std::mt19937 engine;
	std::normal_distribution<float> GDd(0.0f, depth_var_sigma);
	std::normal_distribution<float> GDc(0.0f, 5.0f);
	std::normal_distribution<float> GDdelta(0.0f, 0.010f);
	std::normal_distribution<float> GDalpha(0.0f, 3.0f/180.0f*3.1415f);
	std::vector<Box> boxes;
	for(unsigned int i=0; i<nh; i++) {
		float px = - static_cast<float>(nw) * 0.5f * std::abs(GDdelta(engine));
		for(unsigned int j=0; j<nw; j++) {
			Box box;
			box.position = Eigen::Vector3f(
				px,
				static_cast<float>(i)*box_size.y(),
				GDd(engine)
			);
			float alpha = GDalpha(engine);
			box.orientation = Eigen::Quaternionf(Eigen::AngleAxisf(alpha, Eigen::Vector3f(0.0f, 1.0f, 0.0f)));
			box.size = box_size;
			box.color = (Eigen::Vector3f(135.0f, 90.0f, 48.0f) + Eigen::Vector3f(GDc(engine), GDc(engine), GDc(engine))) / 255.0f;
			boxes.push_back(box);
			float alpha_dx = box.size.x()*std::sin(alpha);
			box.position[0] += alpha_dx;
			px += box_size.x() + alpha_dx +  std::abs(GDdelta(engine));
		}
	}
	return boxes;
}

struct Camera
{
	Eigen::Affine3f pose;
	float fov_deg;
	unsigned int width, height;
};

struct Rgbd
{
	slimage::Image3ub color;
	slimage::Image1ui16 depth;
};

void RenderBox(const Box& box)
{
	auto pose = Eigen::Translation3f(box.position) * box.orientation.matrix();
	float a = 0.5f*box.size.x();
	float b = 0.5f*box.size.y();
	float c = 0.5f*box.size.z();
	Eigen::Vector3f corners[8] = {
		pose*Eigen::Vector3f(-a, -b, -c),
		pose*Eigen::Vector3f(+a, -b, -c),
		pose*Eigen::Vector3f(-a, +b, -c),
		pose*Eigen::Vector3f(+a, +b, -c),
		pose*Eigen::Vector3f(-a, -b, +c),
		pose*Eigen::Vector3f(+a, -b, +c),
		pose*Eigen::Vector3f(-a, +b, +c),
		pose*Eigen::Vector3f(+a, +b, +c),
	};
	unsigned int indices[6][4] = {
		{0,2,3,1}, // top
		{4,5,7,6}, // bottom
		{0,4,6,2}, // left
		{1,3,7,5}, // right
		{0,1,5,4}, // front
		{3,2,6,7}, // back
	};
	Eigen::Vector3f normals[6] = {
		{0,0,+1},
		{0,0,-1},
		{-1,0,0},
		{+1,0,0},
		{0,-1,0},
		{0,+1,0},
	};
	glBegin(GL_QUADS);
	glColor3f(box.color.x(), box.color.y(), box.color.z());
	for(unsigned int k=0; k<6; k++) {
		glNormal3f(normals[k].x(), normals[k].y(), normals[k].z());
		for(unsigned int i : indices[k]) {
			glVertex3f(corners[i].x(), corners[i].y(), corners[i].z());
		}
	}
	glEnd();
}

Rgbd Simulate(const std::vector<Box>& boxes, const Camera& camera)
{
	constexpr float cDepthTolerance = 0.03f;
	constexpr GLenum cDepthInternalFormat = GL_DEPTH_COMPONENT24;
	constexpr double cZNear = 0.5;
	constexpr double cZFar = 30.0;
	constexpr double cDepthMax = 16777215.0;

	unsigned int width = camera.width;
	unsigned int height = camera.height;

	std::cout << "// create OpenGL render buffers" << std::endl;

	// create frame buffer object
	GLuint fb;
	glGenFramebuffers(1, &fb);
	glBindFramebuffer(GL_FRAMEBUFFER, fb);
	// create color buffer and attach to FBO
	GLuint color_rb;
	glGenRenderbuffers(1, &color_rb);
	glBindRenderbuffer(GL_RENDERBUFFER, color_rb);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, color_rb);
	// create depth buffer and attach to FBO
	GLuint depth_rb;
	glGenRenderbuffers(1, &depth_rb);
	glBindRenderbuffer(GL_RENDERBUFFER, depth_rb);
	glRenderbufferStorage(GL_RENDERBUFFER, cDepthInternalFormat, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_rb);

	// check if everything is allright
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE) {
		std::cerr << "glCheckFramebufferStatus says " << status << std::endl;
	}
	else {
		std::cout << "glCheckFramebufferStatus says GL_FRAMEBUFFER_COMPLETE" << std::endl;
	}
	unsigned int glerr = glGetError();
	if(glerr != GL_NO_ERROR) {
		std::cerr << "OpenGL says " << glerr << std::endl;
	}
	else {
		std::cout << "OpenGL says GL_NO_ERROR" << std::endl;
	}

	// render
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluPerspective(camera.fov_deg, float(width)/float(height), static_cast<float>(cZNear), static_cast<float>(cZFar));
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	// opengl uses a different coordinate system so we need to rotate around the x-axis by 180 deg
	Eigen::Matrix4f T_ogl = (
		Eigen::AngleAxisf(boost::math::constants::pi<float>(), Eigen::Vector3f(1,0,0))
		* camera.pose.inverse()).matrix();
	glLoadMatrixf(T_ogl.data());
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
	
	// three point lighting
	float p_ambient[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	float p_spec[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	float p_diffuse_1[4] = {0.90f, 0.90f, 0.90f, 1.0f};
	float p_diffuse_2[4] = {0.45f, 0.45f, 0.45f, 1.0f};
	float p_diffuse_3[4] = {0.60f, 0.60f, 0.60f, 1.0f};
	float p_direction_1[4] = {1.0f, 1.0f, -1.0f, 0.0f};
	float p_direction_2[4] = {1.0f, 1.0f, +1.0f, 0.0f};
	float p_direction_3[4] = {-1.0f, -1.0f, 0.0f, 0.0f};
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, p_ambient);
	glLightfv(GL_LIGHT0, GL_SPECULAR, p_spec);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, p_diffuse_1);
	glLightfv(GL_LIGHT0, GL_POSITION, p_direction_1);
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_AMBIENT, p_ambient);
	glLightfv(GL_LIGHT1, GL_SPECULAR, p_spec);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, p_diffuse_2);
	glLightfv(GL_LIGHT1, GL_POSITION, p_direction_2);
	glEnable(GL_LIGHT2);
	glLightfv(GL_LIGHT2, GL_AMBIENT, p_ambient);
	glLightfv(GL_LIGHT2, GL_SPECULAR, p_spec);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, p_diffuse_3);
	glLightfv(GL_LIGHT2, GL_POSITION, p_direction_3);

	// render boxes
	for(const auto& box : boxes) {
		RenderBox(box);
	}

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	// read result buffer
	slimage::Image4ub buf_color(width, height);
	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buf_color.begin());
	slimage::Image1f buf_depth(width, height);
	glReadPixels(0, 0, width, height, GL_DEPTH_COMPONENT, GL_FLOAT, buf_depth.begin());
	slimage::gui::Show("render color buffer", buf_color, 0);
	slimage::gui::Show("render depth buffer", buf_depth, 1.0f, 200);
	slimage::gui::WaitForKeypress();

	// clean up OpenGL
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteRenderbuffers(1, &depth_rb);
	glDeleteRenderbuffers(1, &color_rb);
	glDeleteFramebuffers(1, &fb);

	// convert OpenGL results to RGB-D
	Rgbd frame;
	frame.color = slimage::Image3ub(camera.width, camera.height);
	for(unsigned int i=0; i<buf_color.size(); i++) {
		const auto& x = buf_color[i];
		frame.color[i] = {{x[0],x[1],x[2]}};
	}
	frame.depth = slimage::Image1ui16(camera.width, camera.height);
	for(unsigned int i=0; i<buf_color.size(); i++) {
		float d_gl = buf_depth[i];
		float zf = (cZNear * cZFar) / (cZFar - d_gl * (cZFar - cZNear));
		uint16_t di = zf > 10.0f ? 0 : static_cast<uint16_t>(zf * 1000.0f);
		//std::cout << d_gl << " -> " << zf << " -> " << di << std::endl;
		frame.depth[i] = di;
	}

	return frame;
}

void Save(const Rgbd& rgbd)
{
	slimage::Save(rgbd.color, "boxes_color.png");
	slimage::Save(rgbd.depth, "boxes_depth.pgm");
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutCreateWindow("GLEW Test");

	GLenum err = glewInit();
	if(GLEW_OK != err) {
		std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
	}
	else {
		std::cout << "Status: Using GLEW " << glewGetString(GLEW_VERSION) << std::endl;
	}

	std::cout << "Preparing scenario..." << std::endl;
	
	auto boxes = CreateBoxWand(5, 10, Eigen::Vector3f(0.3f, 0.2f, 0.4f), 0.1f);
	
	Camera camera;
	camera.pose =
	 	Eigen::AngleAxisf(0.3f, Eigen::Vector3f(0,1,0))
	 	* Eigen::AngleAxisf(0.4f, Eigen::Vector3f(1,0,0))
		* Eigen::Translation3f(0.75f, 1.0f, -2.5f);
	camera.fov_deg = 45.0f;
	camera.width = 640;
	camera.height = 480;
	
	std::cout << "Simulating scenario..." << std::endl;
	
	auto rgbd = Simulate(boxes, camera);

	std::cout << "Saving images..." << std::endl;

	Save(rgbd);

	std::cout << "Finished." << std::endl;

	return 1;
}
